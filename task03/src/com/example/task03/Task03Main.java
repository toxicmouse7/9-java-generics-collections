package com.example.task03;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class Task03Main {

    public static void main(String[] args) throws IOException {

        List<Set<String>> anagrams = findAnagrams(Files.newInputStream(Paths.get("task03/resources/singular.txt")), Charset.forName("windows-1251"));
        for (Set<String> anagram : anagrams) {
            System.out.println(anagram);
        }

    }

    public static String sortString(String string) {
        char[] array = string.toCharArray();
        Arrays.sort(array);
        return Arrays.toString(array);
    }

    public static List<Set<String>> findAnagrams(InputStream inputStream, Charset charset) {
        Map<String, Set<String>> anagrams = new TreeMap<>();

        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, charset))) {
            bufferedReader.lines()
                    .map(String::toLowerCase)
                    .filter(line -> line.length() >= 3 && line.matches("[а-я]*"))
                    .forEach(line ->
                        anagrams.computeIfAbsent(sortString(line), set -> new TreeSet<>()).add(line)
                    );

        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return anagrams
                .values()
                .stream()
                .filter(x -> x.size() >= 2)
                .collect(Collectors.toList());
    }
}
