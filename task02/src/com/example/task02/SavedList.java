package com.example.task02;

import java.io.*;
import java.nio.file.Files;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.LinkedList;

public class SavedList<E extends Serializable> extends AbstractList<E> {

    private final File file;
    private AbstractList<E> list = new LinkedList<>();

    public SavedList(File file) {
        this.file = file;
        if (file.exists())
            loadElements();
    }

    private void loadElements() {
        try (ObjectInputStream ois = new ObjectInputStream(Files.newInputStream(file.toPath()))) {
            list = (AbstractList<E>) ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public E get(int index) {
        return list.get(index);
    }

    @Override
    public boolean contains(Object o) {
        return list.contains(o);
    }

    @Override
    public E set(int index, E element) {
        E result = list.set(index, element);
        dumpElements();

        return result;
    }

    public void dumpElements() {
        try (ObjectOutputStream oos = new ObjectOutputStream(Files.newOutputStream(file.toPath()))) {
            oos.writeObject(list);
        } catch (IOException ioException) {
            throw new RuntimeException(ioException);
        }
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public void add(int index, E element) {
        list.add(index, element);
        dumpElements();
    }

    @Override
    public E remove(int index) {
        E result = list.remove(index);
        dumpElements();

        return result;
    }
}
